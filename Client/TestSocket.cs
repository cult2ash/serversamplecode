﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using System.Collections;

public class TestSocket : MonoBehaviour  {

	public int temp;

	public string sendMessage;

	public class StateObject {
		// Client socket.
		public Socket workSocket = null;
		// Size of receive buffer.
		public const int BufferSize = 256;
		// Receive buffer.
		public byte[] buffer = new byte[BufferSize];
		// Received data string.
		public StringBuilder sb = new StringBuilder();
	}
	// The port number for the remote device.
	private const int port = 25566;

	// ManualResetEvent instances signal completion.


	// The response from the remote device.
	private static String response = String.Empty;

	bool connectDone,sendDone,receiveDone;

	void Start() {
		WriteLine("StartClient");
		StartCoroutine (StartClient());
	}

	void WriteLine(string str, params object[] obj) {

		Debug.Log (string.Format(str, obj));
	}

	IEnumerator StartClient() {
		// Connect to a remote device.

			// Establish the remote endpoint for the socket.
			// The name of the 
			// remote device is "host.contoso.com".
			WriteLine("Start");
		yield return null;
			IPHostEntry ipHostInfo = Dns.GetHostEntry("192.168.2.3");
				IPAddress ipAddress = ipHostInfo.AddressList[0];
			IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
		yield return null;
			// Create a TCP/IP socket.
			Socket client = new Socket(AddressFamily.InterNetwork,
				SocketType.Stream, ProtocolType.Tcp);
		yield return null;
			// Connect to the remote endpoint.
			client.BeginConnect( remoteEP, 
				new AsyncCallback(ConnectCallback), client);

			while(!connectDone) yield return null;

			// Send test data to the remote device.
			Send(client,sendMessage);
			while(!sendDone) yield return null;

			// Receive the response from the remote device.
			Receive(client);
			while(!receiveDone) yield return null;

			// Write the response to the console.
			WriteLine("Response received : {0}", response);

			// Release the socket.
			client.Shutdown(SocketShutdown.Both);
			client.Close();


	}

	void ConnectCallback(IAsyncResult ar) {
		try {
			// Retrieve the socket from the state object.
			Socket client = (Socket) ar.AsyncState;

			// Complete the connection.
			client.EndConnect(ar);

			WriteLine("Socket connected to {0}",
				client.RemoteEndPoint.ToString());

			// Signal that the connection has been made.
			connectDone=true;
		} catch (Exception e) {
			WriteLine(e.ToString());
		}
	}

	void Receive(Socket client) {
		try {
			// Create the state object.
			StateObject state = new StateObject();
			state.workSocket = client;

			// Begin receiving the data from the remote device.
			client.BeginReceive( state.buffer, 0, StateObject.BufferSize, 0,
				new AsyncCallback(ReceiveCallback), state);
		} catch (Exception e) {
			WriteLine(e.ToString());
		}
	}

	void ReceiveCallback( IAsyncResult ar ) {
		try {
			// Retrieve the state object and the client socket 
			// from the asynchronous state object.
			StateObject state = (StateObject) ar.AsyncState;
			Socket client = state.workSocket;

			// Read data from the remote device.
			int bytesRead = client.EndReceive(ar);
			WriteLine("bytesRead="+bytesRead);
			if (bytesRead > 0) {
				// There might be more data, so store the data received so far.
				state.sb.Append(Encoding.ASCII.GetString(state.buffer,0,bytesRead));

				// Get the rest of the data.
				client.BeginReceive(state.buffer,0,StateObject.BufferSize,0,
					new AsyncCallback(ReceiveCallback), state);
				WriteLine(state.sb.ToString());
			} else {
				// All the data has arrived; put it in response.
				if (state.sb.Length > 1) {
					response = state.sb.ToString();
				}

				// Signal that all bytes have been received.
				receiveDone=true;
			}
		} catch (Exception e) {
			WriteLine(e.ToString());
		}
	}

	private void Send(Socket client, String data) {
		// Convert the string data to byte data using ASCII encoding.
		byte[] byteData = Encoding.ASCII.GetBytes(data);

		// Begin sending the data to the remote device.
		client.BeginSend(byteData, 0, byteData.Length, 0,
			new AsyncCallback(SendCallback), client);
	}

	private void SendCallback(IAsyncResult ar) {
		try {
			// Retrieve the socket from the state object.
			Socket client = (Socket) ar.AsyncState;

			// Complete sending the data to the remote device.
			int bytesSent = client.EndSend(ar);
			WriteLine("Sent {0} bytes to server.", bytesSent);

			// Signal that all bytes have been sent.
			sendDone=true;
		} catch (Exception e) {
			WriteLine(e.ToString());
		}
	}


}