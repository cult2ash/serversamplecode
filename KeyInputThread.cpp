//
//  KeyInputThread.cpp
//  SocketServerTest
//
//  Created by Jinwoo Park on 6/23/16.
//  Copyright © 2016 Jinwoo Park. All rights reserved.
//

#include "KeyInputThread.hpp"
#include <string>
#include <iostream>

using namespace std;

void KeyInputThread::run() {
    string str;
    
    while(true) {
       
        getline(cin, str);
        if(str=="help") {
            std::cout << "HELP!" << endl;
        } else
            std::cout << "typing = " << str << endl;
    
    }
}