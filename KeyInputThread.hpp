//
//  KeyInputThread.hpp
//  SocketServerTest
//
//  Created by Jinwoo Park on 6/23/16.
//  Copyright © 2016 Jinwoo Park. All rights reserved.
//

#ifndef KeyInputThread_hpp
#define KeyInputThread_hpp
#include "Thread.hpp"
class KeyInputThread : public Thread
{
    void run();
};

#endif /* KeyInputThread_hpp */
