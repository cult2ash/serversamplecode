// Definition of the ServerSocket class

#ifndef ServerSocket_class
#define ServerSocket_class

#include "Socket.h"
#include "singleton.h"
#include "SocketThread.hpp"

class ServerSocket : private Socket, public Singleton<ServerSocket>
{
public:
    
    ServerSocket ();
    virtual ~ServerSocket();
    
    
    const ServerSocket& operator << ( const std::string& ) const;
    const ServerSocket& operator >> ( std::string& ) const;
    
    void accept ( SocketThread& );
    
};


#endif