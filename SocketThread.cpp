//
//  SocketThread.cpp
//  SocketServerTest
//
//  Created by Jinwoo Park on 6/22/16.
//  Copyright © 2016 Jinwoo Park. All rights reserved.
//

#include "SocketThread.hpp"
// Implementation of the ServerSocket class
#include "ServerSocket.hpp"
#include "SocketException.h"
#include <iostream>


SocketThread::SocketThread() {
    ServerSocket::GetInstance()->accept(*this);
}

SocketThread::~SocketThread()
{
}


const SocketThread& SocketThread::operator << ( const std::string& s ) const
{
    if ( ! Socket::send ( s ) )
    {
        throw SocketException ( "Could not write to socket." );
    }
    
    return *this;
    
}


const SocketThread& SocketThread::operator >> ( std::string& s ) const
{
    if ( ! Socket::recv ( s ) )
    {
        throw SocketException ( "Could not read from socket." );
    }
    
    return *this;
}



void SocketThread::run() {
    std::cout << "in thread recieve....\n";
    
    try
    {
        while ( true )
        {
            std::string data;
            (*this) >> data;
            
            std::cout << data << "\n";
            (*this) << "FADFAddddd";
            
        }
    }
    
    catch ( SocketException& ) {
        std::cout << "disconnect\n";
    }
        
}

