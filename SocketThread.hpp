//
//  SocketThread.hpp
//  SocketServerTest
//
//  Created by Jinwoo Park on 6/22/16.
//  Copyright © 2016 Jinwoo Park. All rights reserved.
//

#ifndef SocketThread_hpp
#define SocketThread_hpp

#include "Socket.h"
#include "Thread.hpp"

class SocketThread : public Socket, public Thread
{
private:
    void Connect();
public:

    SocketThread ();
    virtual ~SocketThread();
    
    void run();
    
    const SocketThread& operator << ( const std::string& ) const;
    const SocketThread& operator >> ( std::string& ) const;
   
};

#endif /* SocketThread_hpp */
