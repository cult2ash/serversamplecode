#ifndef Thread_hpp
#define Thread_hpp
#include <thread>
#include <iostream>
class Thread
{
private:
    std::thread *myThread;

public:    
    Thread ();
    ~Thread ();
    virtual void run ();
    void start ();
    void join ();

};

#endif /* Thread_hpp */
