#include "ServerSocket.hpp"
#include "SocketException.h"
#include "KeyInputThread.hpp"
#include <string>
#include <iostream>

using namespace std;

int main ( int argc, char ** argv )
{
    std::cout << "running....\n";


   
    try
    {
        ServerSocket::GetInstance();
        // Create the socket
        SocketThread *new_sock;
        
        KeyInputThread keyinput;
        keyinput.start();
        
        while ( true )
        {
            new_sock=new SocketThread();
            new_sock->start();
        }
    }
    catch ( SocketException& e )
    {
        std::cout << "Exception was caught:" << e.description() << "\nExiting.\n";
    }
    
    return 0;
}